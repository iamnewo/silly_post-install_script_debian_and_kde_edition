## Why does this exist?
For when I'm too lazy setup Debian GNU/Linux with KDE Plasma and setup everything,
and I just wanted to learn `git`

## What this does.
- Backs up your bashrc, and put mine in, along with my aliases.
- Fix power draw/usage
- Fixes Breeze theme for GTK4/Libadwata apps.
- Fixes flatpak GTK theming issues.
- Installs, and sets up flatpak.
- Installs the following:
	- System:
		- [Auto-cpufreq](https://github.com/AdnanHodzic/autocpufreq)
		- [Btop](https://github.com/aristocratos/btop)
		- [Discover - Flatpak Backend](https://apps.kde.org/discover.flatpak/)
		- [Dust](https://github.com/bootandy/dust)
		- [Eza](https://github.com/eza-community/eza)
		- [FFMPEG](https://ffmpeg.org)
		- [Fzf](https://github.com/junegunn/fzf)
		- [Inkscape](https://inkscape.org)
		- [MPV](https://mpv.io)
		- [Neofetch](https://github.com/dylanaraps/neofetch)
		- [VnStat](https://github.com/vergoh/vnstat)
		- [TLDR](https://tldr.sh/)
		- [VLC](https://videolan.org/vlc)
		- [Wget](https://www.gnu.org/software/wget/)
		- [Yin-Yang](https://github.com/oskarsh/Yin-Yang)
		- [YT-DLP](https://github.com/yt-dlp/yt-dlp)
	- Flatpak:
		- [ElectronMail](https://flathub.org/apps/com.github.vladimiry.ElectronMail)
		- [Element](https://flathub.org/apps/im.riot.Riot)
		- [FreeTube](https://flathub.org/apps/io.freetubeapp.FreeTube)
		- [Jellyfin](https://flathub.org/apps/com.github.iwalton3.jellyfin-media-player)
		- [LibreOffice](https://flathub.org/apps/org.libreoffice.LibreOffice)
		- [LibreWolf](https://flathub.org/apps/io.gitlab.librewolf-community)
		- [LocalSend](https://flathub.org/apps/org.localsend.localsend_app)
		- [Warehouse](https://flathub.org/apps/io.github.flattool.Warehouse)

- Removes a bunch of bloat.
- Runs neofetch, then backs up the config file, and puts mine in.
- Set Krunner to open when Super key is pressed.
- Updates, and upgrades your system.

## How to!
- Install Debian GNU/Linux with KDE Plasma
- Run this codeblock
```
sudo apt install git \
cd ~ \
git clone https://codeberg.org/iamnewo/silly_setup_script.git \
cd silly_setup_script \
chmod +x script.sh \
./script.sh
```
Enter your username's password where prompted, and enjoy!

## Tips
- Run `aliases` inside your terminal to see the various aliases set!
- Run `useful-cmds` inside your terminal to see some useful commands
