#! /bin/bash

########################################################
# Personal script for use when Debian GNU/Linux is     #
# installed though debian-cd live iso with KDE Plasma. #
########################################################

## Initial applications setup, move non-important apps to flatpak, etc.

# Install nala
sudo apt install -y nala

# Remove some pkgs, and install a fake pkg to compensate for the dep. hell
sudo nala -y remove akregator dragonplayer firefox-esr goldendict juk kaddressbook kasumi kate kmail kmousetool kmouth knotes konqueror kontrast korganizer kwrite libreoffice-* mlterm sweeper xiterm+thai xterm
sudo nala install -y kde-plasma-desktop
sudo dpkg -r --force-depends konqueror
sudo dpkg -r --force-depends kde-baseapps
sudo dpkg -r --force-depends kate
sudo dpkg -r --force-depends kwrite
sudo dpkg -r --force-depends kate5-data
sudo dpkg -r --force-depends libkf5konq6
sudo dpkg -i kde-baseapps_420_all.deb

# Install flatpak, and add the flathub remote
sudo nala install -y flatpak
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# Do a system update
sudo nala upgrade -y

# Install nessecary apps
#- Packages installed with nala
sudo nala install -y bat btop ffmpeg fzf gcc inkscape libnotify-bin libsystemd-dev mpv micro neofetch pkg-config plasma-discover-backend-flatpak python3-dev tldr vlc vnstat wget
#- Packages installed with flatpak
flatpak install -y com.github.iwalton3.jellyfin-media-player com.github.vladimiry.ElectronMail im.riot.Riot io.freetubeapp.FreeTube io.gitlab.librewolf-community org.libreoffice.LibreOffice org.localsend.localsend_app org.musicbrainz.Picard
#- Install yt-dlp from source, since the package in Debian's repos are broken 
wget https://github.com/yt-dlp/yt-dlp-master-builds/releases/latest/download/yt-dlp_linux
sudo mv yt-dlp_linux /usr/bin/yt-dlp
#- Install eza for a ls replacement
wget -c https://github.com/eza-community/eza/releases/latest/download/eza_x86_64-unknown-linux-gnu.tar.gz -O - | tar xz
sudo chmod +x eza
sudo chown root:root eza
sudo mv eza /usr/local/bin/eza
#- Install dust as a du replacement
wget https://github.com/bootandy/dust/releases/download/v0.9.0/du-dust_0.9.0-1_amd64.deb
sudo dpkg -i du-dust_0.9.0-1_amd64.deb


# Install Yin-Yang for automated day & night switching
git clone https://github.com/oskarsh/Yin-Yang.git
cd Yin-Yang
./scripts/install.sh
cd ..
rm -rf Yin-Yang


## Flatpak theming concistency

# Change directories the gtk-4.0
cd ~/.config/gtk-4.0

# Rename the gtk4 css file so whatever mods you made to it, stays there
mv gtk.css gtk.css.old

# Curls the patched gtk4 breeze theme
touch gtk.css
curl https://raw.githubusercontent.com/MrCompoopter/Libadwaita-Breeze-Dark/main/gtk.css > gtk.css

# Gives all flatpaks that uses GTK2/3/4 toolkits access to their configs
sudo flatpak override --system --filesystem=xdg-config/gtk-3.0:ro --filesystem=xdg-config/gtkrc-2.0:ro --filesystem=xdg-config/gtk-4.0:ro --filesystem=xdg-config/gtkrc:ro 

# Change dir back to home dir
cd ~/silly_setup_script


## Individual file configs

# Bashrc
mv ~/.bashrc ~/.bashrc_og # Renames the current bashrc file so you have the original file
mv bashrc ~/.bashrc # Moves bashrc from the cloned dir to your home dir

# Aliases list and desc file
mv aliases-DONOTDELETE ~ # Moves the aliases list from the cloned dir to your home dir

# Useful commands list and desc file
mv usefulcmds-DONOTDELETE ~ # Moves the useful commands list from the cloned dir to your home dir

# Neofetch
neofetch  # Runs neofetch to generate a config
cd .config/neofetch  # Changes dirs to .config/neofetch
mv config.conf config.conf.og  # Rename the neofetch file so you have the original, generated config
cd ~/silly_setup_script  # Changes dirs to your home dir
mv config.conf ~/.config/neofetch/config.conf # Moves the config from the cloned dir to neofetch's config dir


## Other KDE shenanigains

# Set the 'Meta' (A.K.A. Super, or Wind*ws key) to open Krunner
kwriteconfig5 --file kwinrc --group ModifierOnlyShortcuts --key Meta "org.kde.krunner,/App,,toggleDisplay"  # Set the shortcut
qdbus-qt5 org.kde.KWin /Kwin reconfigure  # Restart Kwin

## Power management shenanigains
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo !!Make sure to type "i" and press enter when prompted for user input!!
echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
sleep 3
git clone https://github.com/AdnanHodzic/auto-cpufreq.git  # Clone the repo
cd auto-cpufreq && sudo ./auto-cpufreq-installer  # Change dirs, and run the installer script
cd ..  ## Change dirs one level up
rm auto-cpufreq  # Remove the dir that was just cd'ed out of. 

## Let the user know that the script is finished and that their system will reboot.
notify-send "Sily Post-Install Script" "The script is now finished! Rebooting in 10s." && cd ~ && rm -rf silly_post-install_script_debian_and_kde_edition && sleep 10 && systemctl reboot
